# Classes in C++

## What are classes

Classes are a type of structured data, that is stored with functions associated to it e.g.:

```cpp
class vector2D{
public:
	double x, y;
	double scalarProduct(const vector2D &rhs) const {return sqrt(this->x*rhs.x + this->y*rhs.y);}
};
``` 

This very simple class stores two values (`x` and `y`) and allows to form a scalar product with another class of the same type. The following code would do exactly that:

```cpp
vector2D oneVector; // Creates on object of class vector2D
vector2D otherVector; // Creates another object of class vector2D
oneVector.x = 1;
oneVector.y = 1;
otherVector.x = 1;
otherVector.y = 0;
std::cout << "Skalar product of oneVector and otherVector is: " << oneVector.scalarProduct(otherVector) << std::endl;
```

This may not yet seem as a massive gain, but imagine looking at defining specific arithmetic for these objects through what is called operator overloading:

```cpp
friend double operator*(const vector2D& lhs, const vector2D& rhs) {
	return lhs.scalarProduct(rhs);
}
```

You can now simply write: `oneVector * otherVector` and the scalar product will be formed.

To try and compile this yourself, check out `VectorTest.cxx` in this directory and compile with:

```bash
g++ -g -lm -o VectorTest VectorTest.cxx
```

## Why use them

First and foremost, *userfriendlyness*! A well written class will just get used - a set of functions attached to data is more intuitive than a set of functions that can operate on certain data.

Secondly, *encapsulation*. (The old divide and conquer ...)
Objects/Classes encapsulate functionality that belongs to certain data. It'll allow easy identification of problems and modification. Inheritance can add a whole level too that, where abstract objects can all be served with certain functions, whilst specific objects may add functionality, depending on their use and content. For example if you wrote an image class that contains an image and allows saving and storing, that would be, to first order, independent of whether this is a colour image or not. However, a colour image is clearly different from a black and white image and thus has other options on what could be done with it, e.g. colour filters applied. These should only be available to a colour image.

