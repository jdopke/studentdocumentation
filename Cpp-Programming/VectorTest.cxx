#include <iostream>
#include <math.h>

class vector2D{
public:
	double x, y;
	double scalarProduct(const vector2D &rhs) const {return sqrt(this->x*rhs.x + this->y*rhs.y);}
	friend double operator*(const vector2D& lhs, const vector2D& rhs) {
		return lhs.scalarProduct(rhs);
	}
};

int main(int argc, char *argv[]) {
	vector2D oneVector; // Creates on object of class vector2D
	vector2D otherVector; // Creates another object of class vector2D
	oneVector.x = 1;
	oneVector.y = 1;
	otherVector.x = 1;
	otherVector.y = 0;
	std::cout << "Skalar product of oneVector and otherVector is: " << oneVector.scalarProduct(otherVector) << std::endl;
	std::cout << "Another way of forming the scalar product is through plain multiplication" << std::endl;
	std::cout << "oneVector * otherVector = " << oneVector * otherVector << std::endl;
	return 0;
}