# Documentation for early Programmers

I aim to collate a few notes that I've written with respect to either programming technique or minutia, as well as tools that are help in the process. Hopefully this will make it into readable documentation.

Currently this is just notes from what came up in some discussions recently, but maybe I'll update this to run from ReST straight into a proper website with a bit of documentation here and there.

Currently there are markdown (\*.md) documents in the subfolders, feel free to browse - I shall try and add README files to the subfolders as well
