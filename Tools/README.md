# Tools for the average ordinary coder

## GIT (and GitLab)

Git is *the* version control system in fashion these days. Version control systems allow you to keep a copy of your code in different versions as you develop and rolling back to a previous version, should the need ever arise. Whilst this may seem dull at first (and an overhead on what you do), it will make your life better. Check git\_lab.md for more info.

## Crashing code

The GNU Project Debugger: For when your programs crash and you can't find out why with yet-more-print-statements, see the gdb.md in this folder for more info.

## Memory/Performance

valgrind helps understand a programs performance in terms of calls to functions as well as memory footprint and can help see where you are "loosing" memory. To be added, but quite hard as I haven't used it in a while...

