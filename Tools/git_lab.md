# Git and Gitlab

## What's the difference?

Whilst [**Git**](https://git-scm.com/) is a version control system, [**GitLab**](https://about.gitlab.com/) implements a server with additional functionality (code display, automated actions) for Git. Git itself is something one can run locally and save the headache of having lost old versions of code. Gitlab is aimed at collaboration, i.e. multiple users working together remotely.

## Git

Git allows version control through storage of differences between tagged file versions. These differences are kept minimal and compressed, so you won't actually be wasting storage by storing many versions of the same file (provided we're talking about very compressible code files and not pre-compressed binary files like Movies).

To initialise a git repository in a new folder you just go into that folder with your favourite shell (The windows version of git comes with "git bash") and type `git init`.

This will generate a repository for storing files versions in that directory, without any files directly attached. To then add files to that repository you need to first add them for being committed to the repository:

```bash
git add <filename> <2nd filename>
git add <yetAnotherFilenameIForgotEarlier>
```

All these files will then be tagged for being committed. Next run `git commit`:

```bash
git commit -m "Commit Message should go in this hyphenation"
```

The version of these files at the point of running `git commit` is now stored and can be recovered at any time, using a slightly odd git commit tag. Git uses generated hash codes to tag specific versions of a repository. Whilst not easy to memorise, these tags are pretty unique and short enough to send in a text message, so easy to share to point someone to a specific commit.

To automatically add all changed or deleted files in a repository directory, you can directly run git commit with the option `-a`, with no need to use `git add`.

## Git Branching

Feeling comfortable with committing yet? You may have noticed that commiting is very much 1D - you add a new version after a previous version, that's it. Branching allows to create parallel strands of development, which can later be merged into a combined version of the code, e.g. I could be programming a tool allow recognition of letters in an image, either based on a mathematical approach or on e.g. artificial intelligence. To develop both, two people would be helpful. The only thing I'd need to do is declare an interface and then both can write and submit code that will do their part. To allow parallel development, I'd tag a version that is the basis of both and then set up a branch for either development. With developers both basing on a version that is functional, either developer can push their code and develop without affecting the other. Once both are happy with getting back together, the code needs to be merged into one version. A flow chart of this could look like:

```mermaid
gitGraph:
options
{
    "nodeSpacing": 150,
    "nodeRadius": 10
}
end
commit
commit
branch maths
checkout maths
commit
commit
checkout master
commit
checkout maths
commit
checkout master
commit
merge maths
```

## Gitlab

Gitlab is a server side implementation for git repositories. It allows one to `clone` a repository from the gitlab server, and to `push` to and `pull` from that repository, which makes developing code together possible.

If you wondering: This documentation is currently hosted through a gitlab server. It allows users with access to read the files in this repository through a web browser without need for cloning the files to their own computer.

To understand a bit more about gitlab we need to understand a bit more about git: A repository in git is always to full set of files and changes over all time monitored by that git repository. This has certain implications when running common development:

* Cloning always copies the full repository, including all branches (see git branches) of the repository
* Pushing and pulling only happens on a selected branch of the repository but:
	* Push requests will be denied if the current state of the local copy of a branch is not up to date with where it is being pushed to (gitlab in this case) - in other words: If you copied you repositor some months ago, just now came up with a brilliant new line of code that you `git add` and `git commit` locally, then you cannot just push to the remote repository without incorporating all the changes that may have happened there.
	* Sometimes this causes you as the developer extra pain, but common developments are always painful and this version of pain is minimal with respect to problems that would come in if you were not forced to merge with the remote changes first



### CI/CD

Automated actions for continuous integration (CI) and/or continuous deployment can be triggered in gitlab, usually by pushing a new version of the repository to gitlab. I usually try to run all sorts of code checks automatically using CI, like compiling, running all programs with some standard parameters (and example files that should be part of the repository) and possibly summarising code coverage. I use CD type actions for example when automatically generating new documentation from a repository (as I should be doing here) and uploading that to a website.

