# Short GDB intro

## About GDB

### What GDB is

The GNU Project Debugger is a free software tool for debugging binary programs. The most prominent use case is a program crashing and you don't understand the reasons for it. Below is an example on how to run GDB - one of the easiest errors could be you are overwriting memory (accidentally) that is yours, but used for a different purpose. The reaction to that overwritten memory happens later in the code, meaning you cannot just read your code relating to that specific memory and identify why there is a problem - you would need to read ALL your code and make sure it never does anything wrong.

The latter is an admirable goal, but usually falls short of success. Instead, when running into a problem you can run your program using gdb. This will allow you to run the program up to the point where it breaks (or just before then) and identify what happened with your memory (variables). Another way to go is using [watchpoints](https://sourceware.org/gdb/onlinedocs/gdb/Set-Watchpoints.html), I've just sadly never done it myself so cannot give any advice on that yet.

### What GDB is not

To first order: A *simple* solution!

However, once you're at the stage of thinking about using gdb, you've probably got something that isn't easily fixed by printing more things from your code.

### Where to find it

You can find the main project website [here](https://www.gnu.org/software/gdb/). Installing it is usually easiest by taking your favourite linux package manager (also works in Windows Subsystem for Linux) and just installing it

## Running gdb

### Pre-requisites

It is generally wise, whilst debugging to compile you code with an option `-g`, which tells the compiler to attach the sourcecode to the compiled format. This allows programs like GDB to not just run your binary program, but associate the instructions your processor gets with lines of code that you have written. For example a backtrace is more useful when you get a listing of the function calls involved and where things actually crashed, e.g.:

```
(gdb) bt
#0  0x000000000800914f in std::_Rb_tree<...UGLYSTUFF...>::begin (this=0x3ff0000000000000) at /usr/include/c++/7/bits/stl_tree.h:961
#1  0x0000000008008d92 in std::map<...UGLYSTUFF...>::begin (this=0x3ff0000000000000) at /usr/include/c++/7/bits/stl_map.h:349
#2  0x0000000008004d32 in get_delays () at TANG.cxx:37
#3  0x000000000800583a in get_data (f=0x93a4ae0) at TANG.cxx:177
#4  0x0000000008007f8d in main (argc=5, argv=0x7ffffffee028) at TANG.cxx:595
(gdb)
```

Compiling the same program without `-g`, the output looks like this:

```
(gdb) bt
#0  0x000000000800914f in std::_Rb_tree<...UGLYSTUFF...>::begin() ()
#1  0x0000000008008d92 in std::map<...UGLYSTUFF...>::begin() ()
#2  0x0000000008004d32 in get_delays() ()
#3  0x000000000800583a in get_data(TFile*) ()
#4  0x0000000008007f8d in main ()
(gdb)
```

Now if you're lucky, there is only one call to `get_data` and `get_delays` in that code and this backtrace still helps, but line numbers just identify so much more...

## Simple operation

To run gdb with *any program* type the following in the director of *any program*:

```
gdb <any program>
```

This will start gdb, loading the program \<*any-program*\>. You can just run the program then and watch it die by typing `run <command line parameters of any-program>`. To then find out where exactly it crashed type `bt` (BackTrace), which will show the call tree of functions that lead to the crash. If you compiled with `-g`, it'll also show where these corresponding functions where called in your code:

```
(gdb) bt
#0  0x000000000800914f in std::_Rb_tree<...UGLYSTUFF...>::begin (this=0x3ff0000000000000) at /usr/include/c++/7/bits/stl_tree.h:961
#1  0x0000000008008d92 in std::map<...UGLYSTUFF...>::begin (this=0x3ff0000000000000) at /usr/include/c++/7/bits/stl_map.h:349
#2  0x0000000008004d32 in get_delays () at TANG.cxx:37
#3  0x000000000800583a in get_data (f=0x93a4ae0) at TANG.cxx:177
#4  0x0000000008007f8d in main (argc=5, argv=0x7ffffffee028) at TANG.cxx:595
(gdb)
```

In this case a bad memory area was accessed which lead to a so-called segmentation violation. The call tree here is:

> Main Program
> Calls get_data() at line 595
> Calls get_delays() at line 177
> Calls std::map::begin() at line 37
> Further calls are in libraries

To first order the error should be looked for in your code - assume that a library, in particular the std-library, is error free to start with.

To not just let you program crash you can first set a breakpoint where execution will be stopped. All active variables of the program will the be available for you to inspect - in this case I think I did something like:

```
break "TANG.cxx:140"
```

That means every time the program runs into code from line 140 of TANG.cxx, gdb will stop the program execution and allow you to do things, like e.g. print variables that are defined within the code:

```bash
print headersString
```

`headersString` in this example is an object of type `std::map<std::string, std::string>`. You can even access the objects methods:

```bash
print headersString->begin()->first
```

That way you can check what is working. In this case printing worked fine, which meant the loading of headersString had actually worked - however, when running, this code still crashed. (To keep running till the next breakpoint or crash, just type "continue")

​The fact that it loaded fine and still crashed meant that something odd had happened to headersString between loading and using it, so I started looking at the few lines between loading of headersString and it being used. I noticed arrays being indexed with a variable index... (and seeing that I think is an experience value you just have to gain once, so here you go)

​Static C(++) arrays, e.g.:

```cpp
double my_array[10];
```

are basically pointers.
What the above does is declare a pointer my_array and reserve `10*size_of<double>` memory for it, in the location where it is declared, so for example if your program had 3 global variables:

```cpp
double A;
double array[10];
double B;
```

then these end up being addressed next to one another in memory, so `array[11]` is actually the same as `B`, and you could also do crazy things like `(&A)[1]` which addresses the same memory location as `array[0]`. ​Usually nobody does crazy stuff like the second thing, but the former is a lot more likely. Once an array is created one needs to make sure that it is not addressed outside its boundaries (0 to 9 inclusive in this case)
​
The easiest way to avoid such things is by using

```cpp
std::array<double, 10> my_array;
```

rather than

```cpp
double my_array[10];
```

The former is self-aware and will deliver an appropriate error message when indexed out-of-bounds .... I think ... I've never actually tried this...
